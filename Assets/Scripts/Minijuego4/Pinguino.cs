﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pinguino : MonoBehaviour
{
    [SerializeField]
    private movementAnimals pinguino;

    private Transform player;
    private Vector3 target;

    void Start()
    {
        Debug.Log(pinguino.aVelocity);
        player = GameObject.FindGameObjectWithTag("Player").transform;
        target = new Vector3(player.position.x, player.position.y,player.position.z);
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, target, pinguino.aVelocity * Time.deltaTime);

        DestroyObject();
    }
    

    private void DestroyObject()
    {
        Destroy(gameObject , 3f);
    }
}
