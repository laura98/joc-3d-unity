﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdZone : MonoBehaviour
{
    public Bird bird;
    GameObject tempObj;
    // Start is called before the first frame update
    void Start()
    {
        tempObj = GameObject.Find("Pajaro");
        bird = tempObj.GetComponent<Bird>();
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(bird.goPlayer);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Bird")
        {
            bird.goPlayer = true;
        }
    }
}
