﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour
{
    public GameObject Panda;
    public Transform Object;
    float randomTime;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("Wait", 0, 3);

    }
    // Update is called once per frame
    void Update()
    {
        randomTime = Random.Range(1, 5);
    }
  
    void Wait()
    {
        Invoke("spawnPandas",randomTime);
    }
    void spawnPandas()
    {
        Instantiate(Panda, Object);
    }
}
