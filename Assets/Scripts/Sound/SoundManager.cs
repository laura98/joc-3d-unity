﻿using System;
using UnityEngine;
using UnityEngine.Audio;

public class SoundManager : MonoBehaviour {

    public Sound[] sounds;

    private Prota player;

    // Start is called before the first frame update
    void Awake () {
        foreach (Sound s in sounds) {
            s.source = gameObject.AddComponent<AudioSource> ();
            s.source.clip = s.clip;

            s.source.volume = s.volume;
            //s.source.pitch = s.pitch;
            s.source.loop = s.loop;
        }
    }

    void Start () {
        Play ("Theme");

        player = GameObject.FindGameObjectWithTag ("Player").GetComponentInChildren<Prota> ();
    }

    
    public void Play (string name) {
        Sound s = Array.Find (sounds, sound => sound.name == name);
        if (s == null) return;
        s.source.Play ();
    }

    public void Stop (string name) {
        Sound s = Array.Find (sounds, sound => sound.name == name);
        if (s == null) return;
        s.source.Stop ();
    }

}