﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bird : MonoBehaviour
{
    [SerializeField]
    private movementAnimals bird;

    private Transform player;
    private Transform nido;
    private Transform actually;

    public bool goPlayer = true; 


    void Start()
    {
        nido = GameObject.FindGameObjectWithTag("Nido").transform;
        player = GameObject.FindGameObjectWithTag("Player").transform;

    }

    // Update is called once per frame
    void Update()
    {
        if (goPlayer)
        {
            transform.position = Vector3.MoveTowards(transform.position, player.position, bird.aVelocity * Time.deltaTime);
        }
        else
        {
            transform.position = Vector3.MoveTowards(transform.position, nido.position, bird.aVelocity * Time.deltaTime);
        }

    }/*
    private void onTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "LimiteBird")
        {
            Debug.Log("Pa casa");
            goPlayer = false;
        }
    }
    */
}
