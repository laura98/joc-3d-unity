﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectarPlayer : MonoBehaviour
{

    public bool DetectarPlayerBoolean;

    private void OnTriggerEnter(Collider collision){
        
        if (collision.CompareTag("Player")){
            
            DetectarPlayerBoolean = true;
        }
    }

        private void OnTriggerExit(Collider collision){
     
        if (collision.CompareTag("Player")){
            
            DetectarPlayerBoolean = false;
        }
    }
}
