﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monos : MonoBehaviour
{
    [SerializeField]
    private movementAnimals mono;

    static Animator anim;
    public GameObject target;
    private Vector3 vectorMonoToPlayer;

    public GameObject Platano;
    public GameObject firePoint;
    public GameObject DetectarPlayerGO;
    
    private float timeBtwShots;
    public float startTimeBtwShots;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        timeBtwShots = startTimeBtwShots;
    }

    // Update is called once per frame
    void Update()
    {

        bool detectarPlayer = DetectarPlayerGO.GetComponent<DetectarPlayer>().DetectarPlayerBoolean;
        
        vectorMonoToPlayer = new Vector3(target.transform.position.x, this.transform.position.y, target.transform.position.z);
        this.gameObject.transform.LookAt(vectorMonoToPlayer,Vector3.up);

        if (timeBtwShots <= 0){

            if(detectarPlayer){

                anim.SetBool("detectarPlayer",true);
                
                GameObject generarPlatano = Instantiate(Platano, firePoint.transform.position, firePoint.transform.rotation);

                generarPlatano.transform.forward = (target.transform.position - this.transform.position).normalized;

                //Platano platano = platanoGO.GetComponent<Platano>();
                timeBtwShots = startTimeBtwShots;

                //if (platano != null){
                //    platano.Seek(target);
                //}
            }else{
                anim.SetBool("detectarPlayer",false);
            }

        }else{
            timeBtwShots -= Time.deltaTime;
        }
    }

}
