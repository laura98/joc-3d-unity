﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platano : MonoBehaviour
{
    
    private Transform player;
    private Vector3 target;
    
    public Monos monos;

    public GameObject objecToAddForce;
    
    private int speed = 10;
    
    private Vector3 forceDirectionVector;

    private float deleteTimer = 4f;
    
    public void Start(){

        player = GameObject.FindGameObjectWithTag("Player").transform;

        target = monos.transform.forward;
        //target = new Vector3(player.position.x, player.position.y, player.position.z);

    }
    
    void Update()
    {
        
        transform.position += transform.forward * speed * Time.deltaTime;

        Debug.Log(transform.position);

                deleteTimer -= Time.deltaTime;

        if (deleteTimer <= 0f)
        {
            Destroy(this.gameObject);
        }

        //addForce();
    }

    private void OnCollisionEnter(Collision collision){

        //Targetable player = collision.gameObject.GetComponent<Targetable>();
        
        if (player !=null && player.gameObject.tag == "Player"){
            
            //enemy.takeDamage(pistola.damage);
            Destroy(this.gameObject);
        }
    }
    

    /*
    public void Update(){

        if (target == null){
            Destroy(gameObject);
            return;
        }

        Vector3 dir = target.position - transform.position;
        float distanceThisFrame = speed * Time.deltaTime;

        if (dir.magnitude <= distanceThisFrame){
            HitTarget();
            return;
        }

        transform.Translate (dir,normalized * distanceThisFrame, Space.World);

        //addForce();
    }

    void HitTarget(){

        Destroy(target.gameObject);

        Destroy(gameObject);
    }
    */
    /*
    public void addForce(){//Vector3 direction, float _forceMagnitude){

        //Vector3 forceDirectionVector = (target.position - myTransform.position).normalized;
        
        //Vector3 force = Vector3.MoveTowards(transform.position , target, speed * Time.deltaTime);//Vector3.Normalize(target) * speed;
        Vector3 force = new Vector3(Vector3.forward.x, 0f, Vector3.forward.z);

        objecToAddForce.GetComponent<Rigidbody>().AddForce(force, ForceMode.Impulse);

        //transform.position = Vector3.MoveTowards(transform.position, target, forceMagnitude);
    }*/
    
}