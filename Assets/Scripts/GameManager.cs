﻿using System.Collections.Generic;
using UnityEngine;

public enum GameStates
{
    playing,
    pause,
    win,
    mainMenu
}

public class GameManager : MonoBehaviour
{
    public static GameManager _instance;
    public GameStates gameState;
    private GameObject winPanel;
    [HideInInspector]
    public Vector3 lastCheckPointPos;

    [HideInInspector]
    public int lastLevelScene;

    public int hp = 10;
    public int rupies = 0;
    public int keys = 0;
    public int medialHerbs = 0;

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }

        gameState = GameStates.mainMenu;
    }

    void Start()
    {
        
    }

    void Update()
    {
        /*gameStateUpdate();
        if (Input.GetButtonDown("Playing")) {
            gameState = GameStates.playing;
        }*/
    }

    void gameStateUpdate()
    {
        switch (gameState)
        {
            case GameStates.pause:
                setGameInPause();
                break;
            case GameStates.win:
                setGameInWin();
                break;
            case GameStates.mainMenu:
                setGameInMainMenu();
                break;
            default:
                setGameInPlaying();
                break;
        }
    }

    private void setGameInMainMenu()
    { 
        Time.timeScale = 0;    
    }

    private void setGameInPlaying()
    {
        Time.timeScale = 1;
    }

    private void setGameInWin()
    {
        Time.timeScale = 0;
        //winPanel.SetActive(true);
    }

    private void setGameInPause()
    {
        Time.timeScale = 0;
    }

    public GameStates getGameState()
    {
        return this.gameState;
    }

    public void setGameState(GameStates gm)
    {
        this.gameState = gm;
        gameStateUpdate();
    }

    public GameStates GetGameStates()
    {
        return this.gameState;
    }

    public void SaveGame()
    {
        SaveLoadManager.SaveGame(lastLevelScene,lastCheckPointPos);
    }

    public void LoadGame()
    {
        GameData data = SaveLoadManager.LoadGame();

        lastLevelScene = data.levelScene;
        LevelSelector.LoadScene(lastLevelScene);

        lastCheckPointPos.x = data.lastCheckpointPosition[0];
        lastCheckPointPos.y = data.lastCheckpointPosition[1];
        lastCheckPointPos.z = data.lastCheckpointPosition[2];     
    }
}
