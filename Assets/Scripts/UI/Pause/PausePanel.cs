﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PausePanel : MonoBehaviour {
    //[SerializeField]
    public GameObject pausePanel;
    //public GameObject settingsPanel;

    private bool isPaused;

    private void Update () {
        if (Input.GetButtonDown ("P")) {
            isPaused = !isPaused;
        }

        if (isPaused) {
            ActivateMenu ();
        } else {
            DeactivateMenu ();
        }
    }

    public void ActivateMenu () {
        GameManager._instance.setGameState (GameStates.pause);
        pausePanel.SetActive (true);
    }

    public void DeactivateMenu () {
        GameManager._instance.setGameState (GameStates.playing);
        pausePanel.SetActive (false);
    }
    public void OnResumeClicked () {
        isPaused = false;
                //Debug.Log ("resume");

        GameManager._instance.setGameState (GameStates.playing);
    }

    public void OnRestartClicked () {
        SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
        GameManager._instance.setGameState (GameStates.playing);
        Debug.Log ("restart");

    }

    public void OnSettingsClicked () {
        //settingsPanel.SetActive (true);
    }

    public void OnBackToMenuClicked () {
        SceneManager.LoadScene (0);
        GameManager._instance.setGameState (GameStates.mainMenu);
        Debug.Log ("go menu");
    }

    public void OnQuitClicked () {
        Application.Quit ();
    }
}