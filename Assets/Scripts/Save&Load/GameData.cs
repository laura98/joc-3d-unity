﻿﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameData
{
    public int levelScene;
    public float[] lastCheckpointPosition;

    public GameData(int level, Vector3 checkPoint)
    {
        levelScene = level;
        lastCheckpointPosition = new float[3];
        lastCheckpointPosition[0] = checkPoint.x;
        lastCheckpointPosition[1] = checkPoint.y;
        lastCheckpointPosition[2] = checkPoint.z;
    }
}
