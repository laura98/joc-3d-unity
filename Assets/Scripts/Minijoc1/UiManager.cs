﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UiManager : MonoBehaviour {

    public int contadorMonedas = 0;
    public Text textMonedas;

    public Text textVida;
    public GameObject player;

    public Slider healthBar;
    private bool isInPause;
    public Canvas pauseCanvas;

    public Canvas tempsCanvas;

    public Canvas finalCanvas;
    public bool haFinalitzatNivell;

    public Canvas mortCanvas;
    public bool haMort;

    //public Canvas logrosCanvas;

    //public Canvas joystickCanvas;

    // Start is called before the first frame update
    void Start () {
        player = GameObject.FindGameObjectWithTag ("Player");

        isInPause = false;
        pauseCanvas.enabled = isInPause;

        haFinalitzatNivell = false;

        haMort = false;
        mortCanvas.enabled = haMort;

        //joystickCanvas.enabled = false;

        //logrosCanvas.enabled = false;

        finalCanvas.enabled = haFinalitzatNivell;
    }

    // Update is called once per frame
    void Update () {
        int currentHealth = player.GetComponentInChildren<Prota> ().vida;
        int maxHealth = player.GetComponentInChildren<Prota> ().maxVida;

        string vida = currentHealth + "/" + maxHealth;
        string monedas = "x " + contadorMonedas;

        textVida.text = vida;
        textMonedas.text = monedas;

        healthBar.maxValue = maxHealth;
        healthBar.value = currentHealth;

        if (Input.GetKeyDown (KeyCode.P)) {
            isInPause = !isInPause;
            pauseCanvas.enabled = isInPause;
            FindObjectOfType<SoundManager> ().Play ("Pause");
            PauseGame (isInPause, pauseCanvas);
        }
        if (isInPause) {
            Time.timeScale = 0;
            PauseGame (isInPause, pauseCanvas);
        } else {
            Time.timeScale = 1;
        }

        if (haFinalitzatNivell) {
            //tempsCanvas.enabled = true;
            finalCanvas.enabled = true;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }

        if (currentHealth <= 0) haMort = true;

        if (haMort) {
            //animacion muerte
            FindObjectOfType<SoundManager> ().Stop ("Theme");
            FindObjectOfType<SoundManager> ().Play ("Mort");
            mortCanvas.enabled = true;
        }

    }

    public void PauseGame (bool inPause, Canvas canvas) {
        if (inPause) {
            Time.timeScale = 0;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = inPause;
        } else {
            Time.timeScale = 1;
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = inPause;
        }

        canvas.enabled = inPause;
    }

}