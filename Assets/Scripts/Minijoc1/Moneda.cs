﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Moneda : MonoBehaviour {

    GameObject uiManager;

    private GameObject gameManager;

    // Start is called before the first frame update
    void Start () {
        uiManager = GameObject.FindGameObjectWithTag ("UiManager");
        gameManager = GameObject.FindGameObjectWithTag ("GameController");
    }

    void OnTriggerEnter (Collider other) {
        if (other.gameObject.tag == "Player") {
            Debug.Log ("moneda");
            uiManager.GetComponent<UiManager> ().contadorMonedas += 1;
            gameManager.GetComponent<GameControllerLvl1> ().monedas += 1;
            FindObjectOfType<SoundManager> ().Play ("Moneda");
            Destroy (this.gameObject);
        }
    }
}