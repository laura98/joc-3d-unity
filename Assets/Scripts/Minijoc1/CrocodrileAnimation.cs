﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrocodrileAnimation : MonoBehaviour {
    private MovementC cocodril;
    private Animator _anim;

    // Start is called before the first frame update
    void Start () {
        cocodril = transform.parent.GetComponent<MovementC> ();
        _anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update () {
        switch (cocodril.currentState) {
            case MovementC.State.Normal:
                _anim.SetBool ("inPursuit", false);
                _anim.SetBool ("isAttacking", false);
                break;
            case MovementC.State.Pursuit:
                _anim.SetBool ("inPursuit", true);
                _anim.SetBool ("isAttacking", false);
                break;
            case MovementC.State.Atack:
                _anim.SetBool ("inPursuit", false);
                _anim.SetBool ("isAttacking", true);
                break;
            case MovementC.State.BackPosition:
                _anim.SetBool ("inPursuit", true);
                _anim.SetBool ("isAttacking", false);
                break;
        }
    }
}