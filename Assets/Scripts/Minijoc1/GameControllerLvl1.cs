﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameControllerLvl1 : MonoBehaviour {

    public float temps;

    public bool gameHasEnded = false;

    public float restartDelay = 1f;

    public GameObject completeLevelUI;

    public GameObject moneda;

    public float hits = 0;

    public int monedas = 0;

    public GameObject player;

    public Canvas Timer;

    public GameObject manager;

    void Start () {
        player = GameObject.FindGameObjectWithTag ("Player");
        manager = GameObject.FindGameObjectWithTag ("UiManager");
    }

    public void completeLevel () {
        Debug.Log ("LEVEL WON!");
        completeLevelUI.SetActive (true);
    }

    public void EndLevel () {
        if (gameHasEnded == false) {
            gameHasEnded = true;
            Debug.Log ("Game Over");
            Invoke ("Restart", restartDelay);
            Restart ();
        }
    }

    void Restart () {
        SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
    }

    public void Awake () {
        DontDestroyOnLoad (gameObject);
    }

    void Update () {
        int vida = player.GetComponentInChildren<Prota> ().vida;

        hits = hitsCocodril (vida);

        temps += Time.deltaTime;

        monedas = monedesCocodril ();
    }

    public float hitsCocodril (int vida) {
        int res = 0;

        if (vida == 3) res = 0;
        else if (vida == 2) res = 1;
        else if (vida == 1) res = 2;
        else if (vida == 0) res = 3;

        return res;
    }

    
    public int monedesCocodril () {
        return monedas;
    }

    public void Muerte (GameObject player) {
        //GetComponent (player).enabled = false;
        gameObject.SetActive (false);
    }

}