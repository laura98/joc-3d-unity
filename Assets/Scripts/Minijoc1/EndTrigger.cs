using UnityEngine;
using UnityEngine.SceneManagement;

public class EndTrigger : MonoBehaviour {

	public GameControllerLvl1 gameManager;

	public UiManager manager;

	public Canvas canvas;

	void OnTriggerEnter () {

		gameManager.completeLevel ();
		Debug.Log ("lvl complete");
		//canvas.enabled = true;
		manager.haFinalitzatNivell = true;
	}

}