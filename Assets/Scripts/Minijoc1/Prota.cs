﻿    using System.Collections.Generic;
    using System.Collections;
    using System;
    using UnityEngine;

    public class Prota : Character {
        public GameControllerLvl1 gameManager;
        public float maxSpray = 2;
        private Vector3 PrevPos;
        private Vector3 NewPos;
        private Vector3 ObjVelocity;
        public GameObject uiManager;

        public float cooldownTime = 2f;
        private float cooldownRunning = 0f;

        private Canvas dieCanvas;

        // Start is called before the first frame update
        void Start () {
            PrevPos = transform.position;
            NewPos = transform.position;
            vida = maxVida;

            dieCanvas = GameObject.FindGameObjectWithTag ("DieCanvas").GetComponent<Canvas> ();
        }

        // Update is called once per frame
        void Update () {

            if (Input.GetMouseButton (1)) {
                Vector3 spray = Vector3.zero;

                if (ObjVelocity.magnitude > 0) {
                    spray.x = (1 - 2 * UnityEngine.Random.value) * maxSpray;
                    spray.y = (1 - 2 * UnityEngine.Random.value) * maxSpray;
                }

            }
            if (vida <= 0) {
                this.Morir ();
            }
            if (vida > maxVida) {
                vida = maxVida;
            }

            if (cooldownRunning > 0) cooldownTime -= Time.deltaTime;
        }
        void FixedUpdate () {
            NewPos = transform.position;
            ObjVelocity = (NewPos - PrevPos) / Time.fixedDeltaTime;
            PrevPos = NewPos;
        }
        private void Morir () {
            //gameManager.Muerte (this.gameObject);
            dieCanvas.enabled = true;
            FindObjectOfType<SoundManager> ().Stop ("Theme");
            FindObjectOfType<SoundManager> ().Play ("Mort");
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }

        void OnTriggerEnter (Collider other) {
            if (other.gameObject.tag == "Finish") {
                uiManager.GetComponent<UiManager> ().haFinalitzatNivell = false;

                //FindObjectOfType<GameManager> ().EndGame ();
            }

        }

    }