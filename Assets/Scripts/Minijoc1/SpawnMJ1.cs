﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnMJ1 : MonoBehaviour {

    public Transform Spawnpoint;
    public Rigidbody Prefab;

    void OnTriggerEnter () {
        Rigidbody _rb;
        _rb = Instantiate (Prefab, Spawnpoint.position, Spawnpoint.rotation) as Rigidbody;
    }
}