﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerCocodrils : MonoBehaviour {
    public float timeStart;
    public Text textBox;

    bool timerActive = true;

    private GameObject gameManager;

    // Update is called once per frame
    void Update () {
        if (timerActive) {
            timeStart += Time.deltaTime;

            UpdateText ();

            getTime (timeStart);
        }
    }

    private void UpdateText () {
        float s = (timeStart % 60);
        float m = ((int) (timeStart / 60) % 60);
        float h = (int) (timeStart / 3600);

        textBox.text = h.ToString ("00") + " " + m.ToString ("00") + " " + s.ToString ("00");
    }

    public void getTime (float time) {
        //Debug.Log (time);
        gameManager.GetComponent<GameControllerLvl1> ().temps += 1;
    }

}