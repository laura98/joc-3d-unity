﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MovementC : Enemy {

    public NavMeshAgent _nav;
    public State currentState;
    private float minDistance = 1f;
    private Vector3 initialPosition;
    private Vector3 lastPos;
    private float coolDownTime = 3f;
    private float coolDownRemaining = 0f;

    public Animator _anim;

    private UiManager uiManager;

    public enum State {
        Normal,
        Pursuit,
        BackPosition,
        Atack
    }

    // Start is called before the first frame update
    void Start () {
        player = GameObject.FindGameObjectWithTag ("Player");
        _nav = GetComponent<NavMeshAgent> ();
        currentState = State.Normal;
        initialPosition = this.transform.position;
        lastPos = transform.position;
        _anim = this.gameObject.GetComponentInChildren<Animator>();
    }

    // Update is called once per frame
    void Update () {
        switch (currentState) {
            case State.Normal:
                _anim.SetBool ("inPursuit", false);
                _anim.SetBool ("isAttacking", false);
                break;
            case State.Pursuit:
                Comportamiento ();
                break;
            case State.BackPosition:
                BackPosition ();
                break;
            case State.Atack:
                Atack ();
                break;
        }

    }

    protected override void Comportamiento () {
        coolDownRemaining = 0;
        _nav.SetDestination (player.transform.position);
        transform.LookAt (new Vector3 (player.transform.position.x, this.gameObject.transform.position.y, player.transform.position.z));
        _anim.SetBool ("inPursuit", true);
        _anim.SetBool ("isAttacking", false);
    }

    bool ReachPlayer () {
        Vector3 offset = player.transform.position - transform.position;
        float sqrLen = offset.sqrMagnitude;

        if (sqrLen < minDistance) {
            return true;
        } else {
            return false;
        }

    }

    private void BackPosition () {
        _nav.SetDestination (initialPosition);
        transform.LookAt (new Vector3 (initialPosition.x, this.gameObject.transform.position.y, initialPosition.z));

        if (!isMoving ()) {
            currentState = State.Normal;
        }
        _anim.SetBool ("inPursuit", true);
        _anim.SetBool ("isAttacking", false);
    }

    private bool isMoving () {
        Vector3 displacement = transform.position - lastPos;
        lastPos = transform.position;

        if (displacement.magnitude > 0.001) {
            return true;
        } else {
            return false;
        }
    }

    private void Atack () {
        _nav.SetDestination (this.transform.position);
        if (coolDownRemaining <= 0) {
            Debug.Log ("vidadfasdfasdf");
            if(player.gameObject.GetComponentInChildren<Prota> ().vida> 0)
                player.gameObject.GetComponentInChildren<Prota> ().vida -= 1;
            coolDownRemaining = coolDownTime;
        }
        coolDownRemaining -= Time.deltaTime;
        _anim.SetBool ("inPursuit", false);
        _anim.SetBool ("isAttacking", true);
    }

}