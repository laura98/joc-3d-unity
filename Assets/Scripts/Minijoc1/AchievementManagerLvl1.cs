﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AchievementManagerLvl1 : MonoBehaviour {
    public GameObject gameManager;
    public Text time;
    public Text hits;
    public Text monedas;

    public Image timeCheck;
    public Image hitsCheck;
    public Image monedasCheck;
    // Start is called before the first frame update
    void Start () {
        gameManager = GameObject.FindGameObjectWithTag ("GameController");
        GameControllerLvl1 game = gameManager.GetComponent<GameControllerLvl1> ();

        string timeString = "Completa el juego en menos de 5 min " + (int) game.temps + "/" + "3600";
        time.text = timeString;

        string hitsString = "Acaba el juego sin que te cogan   Hits :" + game.hits;
        hits.text = hitsString;

        string monedasString = "Consigue 30 monedas " + game.monedas + "/" + "30";
        monedas.text = monedasString;

        if (game.temps <= 300) {
            timeCheck.enabled = true;
        }

        if (game.hits == 0) {
            hitsCheck.enabled = true;
        }

        if (game.monedas >= 30) {
            monedasCheck.enabled = true;
        }
    }
}