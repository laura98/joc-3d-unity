﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vision : MonoBehaviour {
    private MovementC cocodil;

    // Start is called before the first frame update
    void Start () {
        cocodil = transform.parent.GetComponent<MovementC> ();
    }

    // Update is called once per frame
    void Update () {
    }

    private void OnTriggerEnter (Collider other) {
        if (other.gameObject.tag == "Player") {
            cocodil.currentState = MovementC.State.Pursuit;
            FindObjectOfType<SoundManager> ().Play ("Moviment");
        }
    }

    private void OnTriggerExit (Collider other) {
        if (other.gameObject.tag == "Player") {
            cocodil.currentState = MovementC.State.BackPosition;
        }
    }
}