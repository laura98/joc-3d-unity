﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="New Animal", menuName ="Animal Stat" , order = 51 )]
public class movementAnimals : ScriptableObject
{
    [SerializeField]
    private int animalVelocidad;
    [SerializeField]
    private string nombreAnimal;
    [SerializeField]
    private int damagePlayer;

    public int aVelocity
    {
        get
        {
            return animalVelocidad;
        }
    }

    public string nAnimal
    {
        get
        {
            return nombreAnimal;
        }
    }

    public int dPlayer
    {
        get
        {
            return damagePlayer;
        }
    }

}
