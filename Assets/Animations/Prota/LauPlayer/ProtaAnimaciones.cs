﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProtaAnimaciones : MonoBehaviour {

    Animator anim;
    bool run;
    bool jump;
    bool crouch;

    // Start is called before the first frame update
    void Start () {
        anim = GetComponent<Animator> ();
    }

    // Update is called once per frame
    void Update () {
        float move = Input.GetAxis ("Vertical");
        anim.SetFloat ("walking", move);

        Running ();
        anim.SetBool ("running", run);

        Jump ();
        anim.SetBool ("jump", jump);

        Crouch ();
        anim.SetBool ("crouched", crouch);
    }

    void Running () {
        if (Input.GetKeyDown (KeyCode.LeftShift)) {
            run = true;
        } else if (Input.GetKeyUp (KeyCode.LeftShift)) {
            run = false;
        }
    }

    void Jump () {
        if (Input.GetKeyDown (KeyCode.Space)) {
            jump = true;
        } else if (Input.GetKeyUp (KeyCode.Space)) {
            jump = false;
        }
    }

    void Crouch () {
        if (Input.GetKeyDown (KeyCode.C)) {
            crouch = true;
        } else if (Input.GetKeyUp (KeyCode.C)) {
            crouch = false;
        }
    }
}